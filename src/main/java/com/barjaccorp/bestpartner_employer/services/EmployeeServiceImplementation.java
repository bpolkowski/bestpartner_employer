package com.barjaccorp.bestpartner_employer.services;

import com.barjaccorp.bestpartner_employer.converters.EmployeeConverter;
import com.barjaccorp.bestpartner_employer.converters.EmployeeDtoConverter;
import com.barjaccorp.bestpartner_employer.dto.EmployeeDto;
import com.barjaccorp.bestpartner_employer.entities.Employee;
import com.barjaccorp.bestpartner_employer.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class EmployeeServiceImplementation implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    EmployeeDtoConverter employeeDtoConverter;
    @Autowired
    EmployeeConverter employeeConverter;

    @Override
    public Optional<EmployeeDto> findById(Long id) {
        Optional<Employee> employee = employeeRepository.findById(id);

        if (employee.isPresent()){
            EmployeeDto employeeDto = employeeDtoConverter.apply(employee.get());
            return Optional.of(employeeDto);
        }else {
            return Optional.empty();
        }
    }

    @Override
    public void save(EmployeeDto employeeDto) {

        Employee employee = employeeConverter.apply(employeeDto);
        employeeRepository.save(employee);
    }

    @Override
    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public List<EmployeeDto> findAll() {
        Iterable<Employee> products = employeeRepository.findAll(); // iterable, umożliwia interowanie
        return StreamSupport.stream(products.spliterator(),true).map(employeeDtoConverter)
                .collect(Collectors.toList());
    }
}
