package com.barjaccorp.bestpartner_employer.services;

import com.barjaccorp.bestpartner_employer.dto.EmployeeDto;
import com.barjaccorp.bestpartner_employer.entities.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {


    List<EmployeeDto> findAll();
    Optional<EmployeeDto> findById(Long id);
    void save(EmployeeDto employeeDto);
    void delete (Long id);
}
