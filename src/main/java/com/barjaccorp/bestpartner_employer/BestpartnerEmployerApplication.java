package com.barjaccorp.bestpartner_employer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BestpartnerEmployerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BestpartnerEmployerApplication.class, args);
	}

}
