package com.barjaccorp.bestpartner_employer.dto;

import com.barjaccorp.bestpartner_employer.entities.Character;
import com.barjaccorp.bestpartner_employer.entities.employee_description.*;
import lombok.Data;

import javax.persistence.Id;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

@Data
public class EmployeeDto {


    private Long employeeId;

    @NotNull
    @Size(min=3,max=20)
    private String firstName;

    @NotNull
    @Size(min=3,max=40)
    private String lastName;

    @NotNull
    @Min(18)
    @Max(120)
    private Integer age;
    @NotNull
    private Sex sex;
    @NotNull
    private EducationDegree educationDegree;
    @NotNull
    @Min(60)
    @Max(250)
    private Integer height;
    @NotNull
    @Min(40)
    @Max(500)
    private Integer weight;
    @NotNull
    private BodyType bodyType;
    @NotNull
    private Complexion complexion;
    @NotNull
    private HairColour hairColour;
    @NotNull
    private Ethnicity ethnicity;
    @NotNull
    private ReligiousRelation religiousRelation;
    @NotNull
    private SexualOrientation sexualOrientation;
    @NotNull
    private Availability availability;
    @NotNull
    private WorkPlace workPlace;
    @NotNull
    @Pattern(regexp = "^[0-9]{3} ?[0-9]{3} ?[0-9]{3}$")
    private String phonePrivate;
    @NotNull
    @Pattern(regexp = "^[0-9]{3} ?[0-9]{3} ?[0-9]{3}$")
    private String phoneBusiness;
    @NotNull
    @Email
    private String emailPrivate;
    @Email
    private String emailBusiness;
    private String about;
    @NotNull
    private EmployeeState employeeState;

    private LocalDate insertDate;
    private LocalDate updateDate;

    private List<Character> employeeCharacter;

    public EmployeeDto() {
    }



    public EmployeeDto(Long employeeId, String firstName, String lastName, Integer age, Sex sex, EducationDegree educationDegree, Integer height, Integer weight, BodyType bodyType, Complexion complexion, HairColour hairColour, Ethnicity ethnicity, ReligiousRelation religiousRelation, SexualOrientation sexualOrientation, Availability availability, WorkPlace workPlace, @Pattern(regexp = "^[0-9]{3} ?[0-9]{3} ?[0-9]{3}$") String phonePrivate, @Pattern(regexp = "^[0-9]{3} ?[0-9]{3} ?[0-9]{3}$") String phoneBusiness, @Email String emailPrivate, @Email String emailBusiness, String about, EmployeeState employeeState, LocalDate insertDate, LocalDate updateDate,List<Character> employeeCharacter) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
        this.educationDegree = educationDegree;
        this.height = height;
        this.weight = weight;
        this.bodyType = bodyType;
        this.complexion = complexion;
        this.hairColour = hairColour;
        this.ethnicity = ethnicity;
        this.religiousRelation = religiousRelation;
        this.sexualOrientation = sexualOrientation;
        this.availability = availability;
        this.workPlace = workPlace;
        this.phonePrivate = phonePrivate;
        this.phoneBusiness = phoneBusiness;
        this.emailPrivate = emailPrivate;
        this.emailBusiness = emailBusiness;
        this.about = about;
        this.employeeState = employeeState;
        this.insertDate = insertDate;
        this.updateDate = updateDate;
        this.employeeCharacter = employeeCharacter;
    }




}
