package com.barjaccorp.bestpartner_employer.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CharacterDto {

    private Short characterId;

    private String characterName;


    public CharacterDto(Short characterId, String characterName) {
        this.characterId = characterId;
        this.characterName = characterName;

    }
}
