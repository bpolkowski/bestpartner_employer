package com.barjaccorp.bestpartner_employer.repositories;

import com.barjaccorp.bestpartner_employer.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
