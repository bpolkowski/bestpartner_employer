package com.barjaccorp.bestpartner_employer.repositories;

import com.barjaccorp.bestpartner_employer.entities.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository  extends JpaRepository<Character,Short> {
}
