package com.barjaccorp.bestpartner_employer.converters;

import com.barjaccorp.bestpartner_employer.dto.EmployeeDto;
import com.barjaccorp.bestpartner_employer.entities.Employee;
import com.barjaccorp.bestpartner_employer.entities.employee_description.Availability;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class EmployeeConverter implements Function<EmployeeDto, Employee> {
    @Override
    public Employee apply(EmployeeDto employeeDto) {


        return new Employee(employeeDto.getEmployeeId(),employeeDto.getFirstName(),employeeDto.getLastName(),employeeDto.getAge(),
                employeeDto.getSex(),employeeDto.getEducationDegree(),employeeDto.getHeight(),
                employeeDto.getWeight(),employeeDto.getBodyType(),employeeDto.getComplexion(),
                employeeDto.getHairColour(),employeeDto.getEthnicity(),employeeDto.getReligiousRelation(),
                employeeDto.getSexualOrientation(),employeeDto.getAvailability(),employeeDto.getWorkPlace(),
                employeeDto.getPhonePrivate(),employeeDto.getPhoneBusiness(),employeeDto.getEmailPrivate(),
                employeeDto.getEmailBusiness(),employeeDto.getAbout(),employeeDto.getEmployeeState(),employeeDto.getEmployeeCharacter());
    }
}
