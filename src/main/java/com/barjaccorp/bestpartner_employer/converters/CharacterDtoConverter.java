package com.barjaccorp.bestpartner_employer.converters;

import com.barjaccorp.bestpartner_employer.dto.CharacterDto;
import com.barjaccorp.bestpartner_employer.entities.Character;

import java.util.function.Function;

public class CharacterDtoConverter implements Function<Character, CharacterDto> {
    @Override
    public CharacterDto apply(Character character) {
        return new CharacterDto(character.getCharacterId(), character.getCharacterName());
    }
}
