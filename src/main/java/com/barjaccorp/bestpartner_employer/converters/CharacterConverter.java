package com.barjaccorp.bestpartner_employer.converters;

import com.barjaccorp.bestpartner_employer.dto.CharacterDto;
import com.barjaccorp.bestpartner_employer.entities.Character;

import java.util.function.Function;

public class CharacterConverter implements Function<CharacterDto, Character> {
    @Override
    public Character apply(CharacterDto characterDto) {
        return new Character(characterDto.getCharacterId(),characterDto.getCharacterName());
    }
}
