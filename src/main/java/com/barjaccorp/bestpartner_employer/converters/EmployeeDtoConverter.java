package com.barjaccorp.bestpartner_employer.converters;

import com.barjaccorp.bestpartner_employer.dto.EmployeeDto;
import com.barjaccorp.bestpartner_employer.entities.Employee;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class EmployeeDtoConverter implements Function<Employee, EmployeeDto> {

    @Override
    public EmployeeDto apply(Employee employee) {
        return new EmployeeDto(employee.getEmployeeId(),employee.getFirstName(),employee.getLastName(),
                employee.getAge(),employee.getSex(),employee.getEducationDegree(),employee.getHeight(),
                employee.getWeight(),employee.getBodyType(),employee.getComplexion(),employee.getHairColour(),
                employee.getEthnicity(), employee.getReligiousRelation(),employee.getSexualOrientation(),
                employee.getAvailability(),employee.getWorkPlace(), employee.getPhonePrivate(),
                employee.getPhoneBusiness(),employee.getEmailPrivate(),employee.getEmailBusiness(),
                employee.getAbout(),employee.getEmployeeState(),employee.getInsertDate(),employee.getUpdateDate(), employee.getEmployeeCharacter());
    }
}
