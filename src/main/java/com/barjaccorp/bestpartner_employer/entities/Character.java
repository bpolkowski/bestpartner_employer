package com.barjaccorp.bestpartner_employer.entities;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Character {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Short characterId;
    @Column
    private String characterName;

    @ManyToMany (mappedBy = "employeeCharacter")
    private List<Employee> employees;

    public Character(Short characterId, String characterName) {
        this.characterId = characterId;
        this.characterName = characterName;
    }
}
