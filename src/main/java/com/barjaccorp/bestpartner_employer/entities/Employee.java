package com.barjaccorp.bestpartner_employer.entities;

import com.barjaccorp.bestpartner_employer.entities.employee_description.*;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;


@Data
@NoArgsConstructor
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long employeeId;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private Integer age;
    @Column
    private Sex sex;
    @Column
    @Enumerated(value = EnumType.STRING)
    private EducationDegree educationDegree;
    @Column
    private Integer height;
    @Column
    private Integer weight;
    @Column
    @Enumerated(value = EnumType.STRING)
    private BodyType bodyType;
    @Column
    @Enumerated(value = EnumType.STRING)
    private Complexion complexion;
    @Column
    @Enumerated(value = EnumType.STRING)
    private HairColour hairColour;
    @Column
    @Enumerated(value = EnumType.STRING)
    private Ethnicity ethnicity;
    @Column
    @Enumerated(value = EnumType.STRING)
    private ReligiousRelation religiousRelation;
    @Column
    @Enumerated(value = EnumType.STRING)
    private SexualOrientation sexualOrientation;
    @Column
    @Enumerated(value = EnumType.STRING)
    private Availability availability;
    @Column
    @Enumerated(value = EnumType.STRING)
    private WorkPlace workPlace;
    @Column
    private String phonePrivate;
    @Column
    private String phoneBusiness;
    @Column
    private String emailPrivate;
    @Column
    private String emailBusiness;
    @Column
    private String about;
    @Column
    @Enumerated(value = EnumType.STRING)
    private EmployeeState employeeState;
    @Column
    @CreationTimestamp
    private LocalDate insertDate;
    @Column
    @UpdateTimestamp
    private LocalDate updateDate;

    @ManyToMany
    private List<Character> employeeCharacter;



    public Employee(Long id, String firstName, String lastName, Integer age, Sex sex, EducationDegree educationDegree, Integer height, Integer weight, BodyType bodyType, Complexion complexion, HairColour hairColour, Ethnicity ethnicity, ReligiousRelation religiousRelation, SexualOrientation sexualOrientation, Availability availability, WorkPlace workPlace, String phonePrivate, String phoneBusiness, String emailPrivate, String emailBusiness, String about, EmployeeState employeeState,List<Character> employeeCharacter) {
        this.employeeId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
        this.educationDegree = educationDegree;
        this.height = height;
        this.weight = weight;
        this.bodyType = bodyType;
        this.complexion = complexion;
        this.hairColour = hairColour;
        this.ethnicity = ethnicity;
        this.religiousRelation = religiousRelation;
        this.sexualOrientation = sexualOrientation;
        this.availability = availability;
        this.workPlace = workPlace;
        this.phonePrivate = phonePrivate;
        this.phoneBusiness = phoneBusiness;
        this.emailPrivate = emailPrivate;
        this.emailBusiness = emailBusiness;
        this.about = about;
        this.employeeState = employeeState;
        this.employeeCharacter = employeeCharacter;
    }


}
