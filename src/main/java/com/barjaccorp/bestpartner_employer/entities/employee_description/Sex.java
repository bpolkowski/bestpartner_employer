package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum Sex {
    MAN("mężczyzna"),
    WOMAN("kobieta");

    private final String sex;

    Sex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }
}
