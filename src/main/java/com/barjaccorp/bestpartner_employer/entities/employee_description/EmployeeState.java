package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum EmployeeState {

    ACTIVE("aktywny"),
    INACTIVE("niekatywny"),
    WAITING("czeka na aktywację");

    private final String employeeState;

    EmployeeState(String employeeState) {
        this.employeeState = employeeState;
    }

    public String getEmployeeState() {
        return employeeState;
    }
}
