package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum BodyType {

    FAT("otyły"),
    SLIM("szczupły"),
    MUSCULAR("umięśniony")
    ;

    private final String bodyType;

    BodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getBodyType() {
        return bodyType;
    }




}
