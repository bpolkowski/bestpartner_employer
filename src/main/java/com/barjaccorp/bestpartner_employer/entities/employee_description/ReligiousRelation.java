package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum ReligiousRelation {
    ATHEIST("ateista"),
    CATHOLIC("katolik"),
    MUSLIM("muzułmanin"),
    JUDAISM("judaista"),
    BUDDHIST("buddysta"),
    ORTHODOX("prawosławny"),
    PROTESTANT("protestant");

    private final String religiousRelation;

    ReligiousRelation(String religiousRelation) {
        this.religiousRelation = religiousRelation;
    }

    public String getReligiousRelation() {
        return religiousRelation;
    }
}
