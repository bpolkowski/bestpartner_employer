package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum HairColour {

    BLOND("blond"),
    DARK("czarne"),
    BROWN("brązowe"),
    GRAY("siwe"),
    RED("rude"),
    WHITE("białe");

    private final String hairColour;

    HairColour(String hairColour) {
        this.hairColour = hairColour;
    }

    public String getHairColour() {
        return hairColour;
    }
}
