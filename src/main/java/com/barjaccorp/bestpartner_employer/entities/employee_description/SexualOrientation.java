package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum SexualOrientation {

    HETEROSEXUALNA("heteroseksualna"),
    HOMOSEXUAL("homoseksualna"),
    BISEXUAL("biseksualna ");

    private final String sexualOrientation;

    SexualOrientation(String sexualOrientation) {
        this.sexualOrientation = sexualOrientation;
    }

    public String getSexualOrientation() {
        return sexualOrientation;
    }
}
