package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum EducationDegree {


    NONE("brak"),
    PRIMARY_EDUCATION("podstawowe wykształcenie"),
    SECONDARY_EDUCATION("średnie wykształcenie"),
    HIGHER_EDUCATION("wyższe wykształcenie");


    private final String educationDegree;

    EducationDegree(String educationDegree) {
        this.educationDegree = educationDegree;
    }

    public String getEducationDegree() {
        return educationDegree;
    }
}
