package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum Complexion {

    DARK("ciemna"),
    PALE("jasna"),
    OLIVE("oliwkowa"),
    RED("czerwona"),
    SWARTHY("śniada"),
    YELLOW("żółta");

    private final String complexion;

    Complexion(String complexion) {
        this.complexion = complexion;
    }

    public String getComplexion() {
        return complexion;
    }
}
