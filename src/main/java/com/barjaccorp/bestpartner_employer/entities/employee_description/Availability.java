package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum Availability {


        FULL_TIME("cały etat"),
        PART_TIME("pół etatu");

        private final String availability;

        private Availability(String availability){
            this.availability = availability;
        }

         public String getAvailability() {
            return availability;
    }
}
