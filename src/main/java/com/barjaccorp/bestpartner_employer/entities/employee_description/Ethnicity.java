package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum Ethnicity {

    ASIAN("azjatyckie"),
    AFRICAN("afrykańskie"),
    AMERICAN("amerykańskie"),
    INDIAN("indyjskie"),
    CAUCASOID("europeidalne");

    private final String ethnicity;

    Ethnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getEthnicity() {
        return ethnicity;
    }
}
