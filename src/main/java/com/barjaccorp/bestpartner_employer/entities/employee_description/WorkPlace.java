package com.barjaccorp.bestpartner_employer.entities.employee_description;

public enum WorkPlace {

    WARSAW("Warszawa"),
    WROCLAW("Wrocław"),
    TRICITY("Trójmiasto");

    private final String workPlace;

    WorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    public String getWorkPlace() {
        return workPlace;
    }
}
