package com.barjaccorp.bestpartner_employer.controllers.web;


import com.barjaccorp.bestpartner_employer.dto.EmployeeDto;
import com.barjaccorp.bestpartner_employer.entities.Character;
import com.barjaccorp.bestpartner_employer.entities.employee_description.BodyType;
import com.barjaccorp.bestpartner_employer.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Controller
public class EmployeeWebController {

    @Autowired
    MessageSource messageSource;
    @Autowired
    EmployeeService employeeService;


    @GetMapping("/employees/list")
    public String showEmployeesList(Model model) {
        String emptyEmployeesList = messageSource.getMessage("empty.employees.list",new Object[] {},Locale.getDefault());
        List<EmployeeDto> employees = employeeService.findAll();
        if (employees.isEmpty()){
            model.addAttribute("errorMessage", emptyEmployeesList);

        }else {
            model.addAttribute("employees", employees);
        }
        return "employeesList";
    }

    @GetMapping("/employee")
    public String showEmployeeById(@RequestParam (name = "employeeId") Long id, Model model){
        String userNotFoundMessage = messageSource.getMessage("user.not.found",new Object[] {id},
                Locale.getDefault());
        Optional<EmployeeDto> employeeDto = employeeService.findById(id);
        if (employeeDto.isPresent()){
            model.addAttribute("employee", employeeDto);
        }else{
            model.addAttribute("errorMessage", userNotFoundMessage);
        }
        return "addingEmployee";
    }

    @PostMapping("/employee/save")
    public String saveEmployee( @Valid @ModelAttribute(name = "employee") EmployeeDto employeeDto, BindingResult bindingResult, Model model){
        if (bindingResult.hasErrors()){
            model.addAttribute("employee",employeeDto);
            return "addingEmployee";
        }
        employeeService.save(employeeDto);
        return "redirect:/employees/list";
    }

    @GetMapping(value = "/employee/add")
    public String addEmployee(Model model){
        EmployeeDto employeeDto = new EmployeeDto();
        model.addAttribute("employee", employeeDto);
        return "addingEmployee";
    }

    @GetMapping(value = "/employee/delete")
    public String deleteEmployee(@RequestParam(name = "employeeId") Long id){
        employeeService.delete(id);
        return "redirect:/employees/list";
    }

}
