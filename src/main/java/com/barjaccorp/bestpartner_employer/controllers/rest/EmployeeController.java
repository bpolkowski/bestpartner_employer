package com.barjaccorp.bestpartner_employer.controllers.rest;


import com.barjaccorp.bestpartner_employer.dto.EmployeeDto;
import com.barjaccorp.bestpartner_employer.exceptions.EmployeeNotFoundException;
import com.barjaccorp.bestpartner_employer.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployeeController {



    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value = "/employee/restList")
    public List<EmployeeDto> findAll(){
        return employeeService.findAll();
    }

    @GetMapping(value = "/employee/{employeeId}")
    public Optional<EmployeeDto> findById(@PathVariable(name = "employeeId") Long id){
        Optional<EmployeeDto> employeeDto = employeeService.findById(id);
        if (employeeDto.isPresent()){
            return employeeDto;
        }else {
            throw new EmployeeNotFoundException("Employee with id=" + id + " was not found");
        }
    }

}
